//
//  AppDelegate.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    private let style = Style.main
    private lazy var appCoordinator: AppCoordinator = {
        let coordinatorFactory = GeneralAppCoordinatorFactory(style: style)
        return AppCoordinator(style: style,
                              appCoordinatorFactory: coordinatorFactory)
    }()

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {

        appCoordinator.start()
        ColorCubeStorage.default.loadFilters()

        return true
    }
}
