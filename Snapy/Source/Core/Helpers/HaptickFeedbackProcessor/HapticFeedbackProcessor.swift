//
//  HapticFeedbackProcessor.swift
//  Snapy
//
//  Created by Игорь Талов on 25/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

func performMediumImpactHapticFeedback() {
    performImpactHapticFeedback(with: .medium)
}

func performLightImpactHapticFeedback() {
    performImpactHapticFeedback(with: .light)
}

func performSuccessNotificationHapticFeedback() {
    performNotificationHapticFeedback(with: .success)
}

func performFailureNotificationHapticFeedback() {
    performNotificationHapticFeedback(with: .error)

}

func performWarningNotificationHapticFeedback() {
    performNotificationHapticFeedback(with: .warning)
}

func performSelectionHapticFeedback() {
    performSelectionChangedHapticFeedback()
}

func performImpactHapticFeedback(with style: UIImpactFeedbackGenerator.FeedbackStyle) {
    let feedbackGenerator = UIImpactFeedbackGenerator(style: style)
    feedbackGenerator.prepare()
    feedbackGenerator.impactOccurred()
}

func performNotificationHapticFeedback(with notificationType: UINotificationFeedbackGenerator.FeedbackType) {
    let feedbackGenerator = UINotificationFeedbackGenerator()
    feedbackGenerator.prepare()
    feedbackGenerator.notificationOccurred(notificationType)
}

func performSelectionChangedHapticFeedback() {
    let feedbackGenerator = UISelectionFeedbackGenerator()
    feedbackGenerator.selectionChanged()
}
