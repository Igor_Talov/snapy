//
//  OperationQueue+InitWithName.swift
//  Snapy
//
//  Created by Игорь Талов on 30/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

extension OperationQueue {
    convenience init(name: String) {
        self.init()
        self.name = name
    }
}
