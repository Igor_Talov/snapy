//
//  GeneralAppCoordinatorFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralAppCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralAppCoordinatorFactory: AppCoordinatorFactory {
    func makeStartScreenCoordinator() -> StartsScreenCoordinator {
        let moduleFactory = GeneralStartScreenModuleFactory(style: style)
        let coordinatorFactory = GeneralStartScreenCoordinatorFactory(style: style)
        let coordinator = GeneralStartScreenCoordinator(style: style,
                                                        moduleFactory: moduleFactory,
                                                        coordinatorfactory: coordinatorFactory)
        return coordinator
    }

    func makeStudioCoordinator() -> StudioCoordinator {
        let moduleFactory = GeneralStudioModuleFactory(style: style)
        let coordinatorFactory = GeneralStudioCoordinatorFactory(style: style)
        let coordinator = GeneralStudioCoordinator(style: style,
                                                   moduleFactory: moduleFactory,
                                                   coordinatorFactory: coordinatorFactory)
        return coordinator
    }

    func makeCameraCoordinator() -> CameraCoordinator {
        let moduleFactory = GeneralCameraModuleFactory(style: style)
        let coordinator = GeneralCameraCoordinator(style: style,
                                                   moduleFactory: moduleFactory)
        return coordinator
    }
}
