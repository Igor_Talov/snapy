//
//  AppCoordinatorFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol AppCoordinatorFactory {
    func makeStudioCoordinator() -> StudioCoordinator
    func makeStartScreenCoordinator() -> StartsScreenCoordinator
}
