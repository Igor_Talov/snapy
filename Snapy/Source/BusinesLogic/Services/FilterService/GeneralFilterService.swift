//
//  GeneralFilterService.swift
//  Snapy
//
//  Created by Игорь Талов on 23/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralFilterService: FilterService {
    // MARK: - Private properties
    private let filtersStorage: ColorCubeStorage

    // MARK: - Initialization
    init(filtersStorage: ColorCubeStorage) {
        self.filtersStorage = filtersStorage
    }

    func getColorCubeFilters() -> [ColorCubeFilter] {
        return filtersStorage.filters
    }
}
