//
//  ColorCubeStorage.swift
//  Snapy
//
//  Created by Игорь Талов on 22/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class ColorCubeStorage {
    static let `default` = ColorCubeStorage(filters: [])

    var filters: [ColorCubeFilter] = []

    init(filters: [ColorCubeFilter]) {
        self.filters = filters
    }
}

extension ColorCubeStorage {
    func loadFilters() {
        do {
            let bundle = Bundle.main
            let rootPath = bundle.bundlePath as NSString

            let fileList = try FileManager.default.contentsOfDirectory(atPath: rootPath as String)
            let filters = try fileList.filter { $0.hasSuffix(".png") }
                .sorted()
                .compactMap { path -> ColorCubeFilter? in
                    let url = URL(fileURLWithPath: rootPath.appendingPathComponent(path))
                    let data = try Data(contentsOf: url)
                    let name = path.replacingOccurrences(of: ".png", with: "")
                                   .replacingOccurrences(of: "PF_", with: "")
                    if let image = UIImage(data: data) {
                        return ColorCubeFilter(name: name,
                                               identifier: path,
                                               lutImage: image,
                                               dimension: 64)
                    } else { return nil }
                }
            self.filters = filters
            // Dirty hack...
            self.filters.remove(at: 0)
        } catch let error as NSError {
            fatalError(error.localizedDescription)
        }
    }
}
