//
//  ColorCube.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import UIKit

public struct ColorCube {

    public func makeLutFilter(from lutImage: UIImage,
                              dimension: Int = 64,
                              colorSpace: CGColorSpace) -> CIFilter {
        guard let data = getCubeData(from: lutImage, dimension: dimension, colorSpace: colorSpace) else { return CIFilter() }

        let parameters = ["inputCubeDimension": dimension,
                          "inputCubeData": data,
                          "inputColorSpace": colorSpace] as [String: Any]
        guard let filter = CIFilter(name: "CIColorCubeWithColorSpace", parameters: parameters) else { return CIFilter() }

        return filter
    }

    private func createBitmap(image: CGImage, colorSpace: CGColorSpace) -> UnsafeMutablePointer<UInt8>? {
        let width    = image.width
        let height   = image.height
        let bitsPerComponent = 8
        let bytesPerRow = width * 4

        let bitmapSize = height * bytesPerRow

        let bitmapInfo = CGImageAlphaInfo.premultipliedLast.rawValue
        guard let data = malloc(bitmapSize) else {
            return nil
        }

        guard let context = CGContext(data: data,
                                      width: width,
                                      height: height,
                                      bitsPerComponent: bitsPerComponent,
                                      bytesPerRow: bytesPerRow,
                                      space: colorSpace,
                                      bitmapInfo: bitmapInfo,
                                      releaseCallback: nil,
                                      releaseInfo: nil) else { return nil }

        context.draw(image, in: CGRect(x: 0, y: 0, width: width, height: height))
        return data.bindMemory(to: UInt8.self, capacity: bitmapSize)
    }

    private func getCubeData(from image: UIImage,
                             dimension: Int,
                             colorSpace: CGColorSpace) -> Data? {

        guard let cgImage = image.cgImage else {
            return nil
        }

        guard let bitmap = createBitmap(image: cgImage, colorSpace: colorSpace) else { return nil }

        let width = cgImage.width
        let height = cgImage.height
        let rowNum = width / dimension
        let columnNum = height / dimension
        let dataSize = dimension * dimension * dimension * MemoryLayout<Float>.size * 4

        var array = [Float](repeating: 0, count: dataSize)

        var bitmapOffest: Int = 0
        var zAxis: Int = 0

        for _ in stride(from: 0, to: rowNum, by: 1) {
            for yAxis in stride(from: 0, to: dimension, by: 1) {
                let tmp = zAxis
                for _ in stride(from: 0, to: columnNum, by: 1) {
                    for xAxis in stride(from: 0, to: dimension, by: 1) {
                        let dataOffset = (zAxis * dimension * dimension + yAxis * dimension + xAxis) * 4

                        let position = bitmap
                            .advanced(by: bitmapOffest)

                        array[dataOffset + 0] = Float(position
                            .advanced(by: 0)
                            .pointee) / 255

                        array[dataOffset + 1] = Float(position
                            .advanced(by: 1)
                            .pointee) / 255

                        array[dataOffset + 2] = Float(position
                            .advanced(by: 2)
                            .pointee) / 255

                        array[dataOffset + 3] = Float(position
                            .advanced(by: 3)
                            .pointee) / 255

                        bitmapOffest += 4

                    }
                    zAxis += 1
                }
                zAxis = tmp
            }
            zAxis += columnNum
        }

        free(bitmap)

        let data = Data(bytes: array, count: dataSize)
        return data
    }
}
