//
//  ColorCubeFilter.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import Foundation
import UIKit

class ColorCubeFilter {
    let cifilter: CIFilter
    let name: String
    let identifier: String
    private(set) var amount: Double = 1

    init(name: String,
         identifier: String,
         lutImage: UIImage,
         dimension: Int) {
        self.name = name
        self.identifier = identifier
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        self.cifilter = ColorCube().makeLutFilter(from: lutImage,
                                                  dimension: dimension,
                                                  colorSpace: colorSpace)
    }
}

extension ColorCubeFilter: Filterable {
    func apply(to image: CIImage, sourceImage: CIImage) -> CIImage? {
        let filter = self.cifilter

        filter.setValue(image, forKeyPath: kCIInputImageKey)
        if let colorSpace = image.colorSpace {
            filter.setValue(colorSpace, forKeyPath: "inputColorSpace")
        }

        let background = image
        // swiftlint:disable force_unwrapping
        let foreground = filter.outputImage!.applyingFilter(
            "CIColorMatrix", parameters: [
                "inputRVector": CIVector(x: 1, y: 0, z: 0, w: 0),
                "inputGVector": CIVector(x: 0, y: 1, z: 0, w: 0),
                "inputBVector": CIVector(x: 0, y: 0, z: 1, w: 0),
                "inputAVector": CIVector(x: 0, y: 0, z: 0, w: CGFloat(amount)),
                "inputBiasVector": CIVector(x: 0, y: 0, z: 0, w: 0)])

        let cimagesComposition = CIFilter(
            name: "CISourceOverCompositing",
            parameters: [
                kCIInputImageKey: foreground,
                kCIInputBackgroundImageKey: background
            ])!
        // swiftlint enable: force_unwrapping
        return cimagesComposition.outputImage
    }
}
