//
//  Filterable.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage

protocol Filterable {
    func apply(to image: CIImage, sourceImage: CIImage) -> CIImage?
}
