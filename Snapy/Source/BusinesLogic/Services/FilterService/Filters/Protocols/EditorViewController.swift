//
//  EditorViewController.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let cellReuseIdentifier = "FilterViewCell"
    static let filterPreviewCellWidth: CGFloat = 80.0
    static let filterPreviewCellHeight: CGFloat = 90.0
}

class EditorViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Public properties
    var output: EditorViewOutput?
    var filtersViewModels = [FilterViewModel]()

    // MARK: - Outlets
    @IBOutlet private weak var collectionView: UICollectionView?
    @IBOutlet private weak var imageView: UIImageView?
    @IBOutlet private weak var doneButton: UIButton?

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)),
                   bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
        configureCollectionView()
    }
}

private extension EditorViewController {
    func configureCollectionView() {
        collectionView?.register(UINib(nibName: FilterCollectionViewCell.nibName,
                                       bundle: nil),
                                 forCellWithReuseIdentifier: Constants.cellReuseIdentifier)
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
}

extension EditorViewController: EditorViewInput {
    func displayFilters(viewModels: [FilterViewModel]) {
        safelyRunUICode {
            self.filtersViewModels = viewModels
            self.collectionView?.reloadData()
        }
    }

    func displayTools() { }

    func displaySourceImage(sourceImage: CIImage) {
        safelyRunUICode {
            let uiimage = UIImage(ciImage: sourceImage)
            self.imageView?.image = uiimage
        }
    }
}

extension EditorViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filtersViewModels.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellReuseIdentifier,
                                                      for: indexPath) as? FilterCollectionViewCell

        let viewModel = filtersViewModels[indexPath.row]
        cell?.style = style
        cell?.viewModel = viewModel
        return cell ?? UICollectionViewCell()
    }
}

extension EditorViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: Constants.filterPreviewCellWidth,
                      height: Constants.filterPreviewCellHeight)
    }
}

extension EditorViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let viewModel = filtersViewModels[indexPath.row]
        viewModel.action?(viewModel.filter)
    }
}
