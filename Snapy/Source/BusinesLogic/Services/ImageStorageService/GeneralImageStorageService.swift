//
//  GeneralImageStorageService.swift
//  Snapy
//
//  Created by Игорь Талов on 22/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import Foundation

private struct Constant {
    static let folderName = "Snappy"
}

class GeneralImageStorageService {
    init() {
        createImageDirectoryIfNotExist()
    }
}

private extension GeneralImageStorageService {
    func imageDirectoryPath() -> String {
       let tempDirPath = NSTemporaryDirectory() as NSString
       return tempDirPath.appendingPathComponent(Constant.folderName)
    }

    func randomTemporaryImageFilePath() -> String {
       let tempName = "\(NSUUID().uuidString).jpg"
       let directoryPath = self.imageDirectoryPath() as NSString
       return directoryPath.appendingPathComponent(tempName)
    }

    func createImageDirectoryIfNotExist() {
        var isDirectory: ObjCBool = false
        let path = imageDirectoryPath()
        let exist = FileManager.default.fileExists(atPath: path,
                                                   isDirectory: &isDirectory)
        if !exist || !isDirectory.boolValue {
            do {
                try FileManager.default.createDirectory(atPath: imageDirectoryPath(),
                                                        withIntermediateDirectories: false,
                                                        attributes: nil)
            } catch {
                assertionFailure("Couldn't create folder for images with error: \(error)")
            }
        }
    }

    func imageDirectoryContent(completion: @escaping ([PhotoFromCamera]?) -> Void) {
        let directoryPath = imageDirectoryPath() as NSString
        let filePaths: [String]
        do {
            filePaths = try FileManager.default.contentsOfDirectory(atPath: directoryPath as String)
            if !filePaths.isEmpty {
                // TODO: Need rename 🤔
                let photosItems = filePaths.map { filePath -> PhotoFromCamera in
                    let fullPath = directoryPath.appendingPathComponent(filePath)
                    return PhotoFromCamera(path: fullPath)
                }
                completion(photosItems)
            }
        } catch {
            assertionFailure("can't get file paths for directory \(directoryPath), error: \(error)")
        }

    }
}

extension GeneralImageStorageService: ImageStorageService {

    func getPhotos(completion: @escaping ([PhotoFromCamera]?) -> Void) {
        imageDirectoryContent(completion: completion)
    }

    func save(sampleBuffer: CMSampleBuffer?,
              callbackQueue: DispatchQueue,
              completion: @escaping (String?) -> Void) {
        createImageDirectoryIfNotExist()
        DispatchQueue.global(qos: .userInitiated).async {
            let imageData = sampleBuffer.flatMap { AVCapturePhotoOutput
                .jpegPhotoDataRepresentation(forJPEGSampleBuffer: $0,
                                             previewPhotoSampleBuffer: nil)
            }
            var destination: String?
            if let imageData = imageData {
                let path = self.randomTemporaryImageFilePath()
                do {
                    try imageData.write(to: URL(fileURLWithPath: path),
                                        options: [.atomicWrite])
                    destination = path
                } catch {
                    assertionFailure("Couldn't save photo at path \(path) with error: \(error)")
                }
            }
            callbackQueue.async {
                completion(destination)
            }
        }
    }

    func save(_ image: CGImage) -> String? {
        // TODO: Implement Save
        return ""
    }

    func remove(_ path: String) {}

    func removeAll() {}
}
