//
//  ImageStorageService.swift
//  Snapy
//
//  Created by Игорь Талов on 22/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import Foundation
import ImageIO

protocol ImageStorageService: class {
    func save(sampleBuffer: CMSampleBuffer?,
              callbackQueue: DispatchQueue,
              completion: @escaping (String?) -> Void
    )
    func getPhotos(completion: @escaping ([PhotoFromCamera]?) -> Void)
    func save(_ image: CGImage) -> String?
    func remove(_ path: String)
    func removeAll()
}
