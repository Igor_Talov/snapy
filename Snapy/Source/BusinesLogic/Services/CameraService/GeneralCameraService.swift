//
//  GeneralCameraService.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import ImageSource

public enum CameraType {
    case back
    case front
}

class GeneralCameraService: NSObject {
    // MARK: - Private properties
    private let imageStorageService: ImageStorageService

    private var captureSession: AVCaptureSession?
    private var output: AVCapturePhotoOutput?
    private var backCamera: AVCaptureDevice?
    private var frontCamera: AVCaptureDevice?

    private var capturePhotoBlock: ((PhotoFromCamera?) -> Void)?

    private var activeCamera: AVCaptureDevice? {
        return camera(for: activeCameraType)
    }

    private var activeCameraType: CameraType

    private let workQoueue = DispatchQueue(label: "com.SealDev.CameraServiceQueue")

    // MARK: - Initialization
    init(cameraType: CameraType,
         imageStorageService: ImageStorageService) {
        self.activeCameraType = cameraType
        self.imageStorageService = imageStorageService
        super.init()
        let discoverySession = makeDiscoverySession()

        backCamera = discoverySession.devices.filter { $0.position == .back }.first
        frontCamera = discoverySession.devices.filter { $0.position == .front }.first
    }
}

private extension GeneralCameraService {
    func camera(for cameraType: CameraType) -> AVCaptureDevice? {
        switch cameraType {
        case .back:
            return backCamera
        case .front:
            return frontCamera
        }
    }

    func makeDiscoverySession() -> AVCaptureDevice.DiscoverySession {
        let devices: [AVCaptureDevice.DeviceType] = [.builtInDualCamera, .builtInTelephotoCamera, .builtInWideAngleCamera]
        let session = AVCaptureDevice.DiscoverySession(deviceTypes: devices, mediaType: .video, position: .unspecified)
        return session
    }

    func setupCaptureSession() {
        do {
            #if arch(i386) || arch(x86_64)
                assertionFailure("Crash in simulator")
            #endif

            guard let activeCamera = activeCamera else {
                assertionFailure("active camera is nil")
                return
            }

            let captureSession = AVCaptureSession()
            captureSession.sessionPreset = .photo

            let photoInput = try AVCaptureDeviceInput(device: activeCamera)
            let photoOutput = AVCapturePhotoOutput()
            let settingsArray = [AVCapturePhotoSettings(format: [AVVideoCodecKey: AVVideoCodecType.jpeg])]
            photoOutput.setPreparedPhotoSettingsArray(settingsArray,
                                                      completionHandler: nil)
            if captureSession.canAddInput(photoInput) && captureSession.canAddOutput(photoOutput) {
                captureSession.addInput(photoInput)
                captureSession.addOutput(photoOutput)
            } else {
                assertionFailure("capture session can't add input & output")
            }

            captureSession.startRunning()

            self.captureSession = captureSession
            self.output = photoOutput
        } catch {
            self.captureSession = nil
            self.output = nil
        }
    }

    func orientationForCamera(activeCamera: AVCaptureDevice?) -> ExifOrientation {
        if activeCamera == frontCamera {
            return .leftMirrored
        } else {
            return .left
        }
    }

    func videoOutputConnection() -> AVCaptureConnection? {
        guard let output = output else {
            return nil
        }

        for connection in output.connections {
            let inputPorts = connection.inputPorts

            if !inputPorts.isEmpty {
                _ = inputPorts.filter { $0.mediaType == .video }
                return connection
            }
        }
        return nil
    }

    func avOrientationForCurrentDeviceOrientation() -> AVCaptureVideoOrientation {
        switch UIDevice.current.orientation {
        case .portrait:
            return .portrait
        case .portraitUpsideDown:
            return .portraitUpsideDown
        case .landscapeLeft:
            return .landscapeRight
        case .landscapeRight:
            return .landscapeLeft
        default:
            return .portrait
        }
    }
}

extension GeneralCameraService: CameraService {
    var isFlashAvailable: Bool {
        return true
    }

    var isFlashEnabled: Bool {
        return true
    }

    func getCaptureSession(completion: @escaping (AVCaptureSession?) -> Void) {
        workQoueue.async { [weak self] in
            guard let self = self else {
                return
            }
            if let captureSession = self.captureSession {
                safelyRunUICode {
                    completion(captureSession)
                }
            } else {
                let mediaType = AVMediaType.video
                let authorizationStatus = AVCaptureDevice.authorizationStatus(for: mediaType)

                switch authorizationStatus {
                case .authorized:
                    self.setupCaptureSession()
                    completion(self.captureSession)
                case .notDetermined:
                    AVCaptureDevice.requestAccess(for: mediaType) { granted in
                        self.workQoueue.async {
                            if let captureSession = self.captureSession {
                                safelyRunUICode {
                                    completion(captureSession)
                                }
                            } else if granted {
                                self.setupCaptureSession()
                                safelyRunUICode {
                                    completion(self.captureSession)
                                }
                            } else {
                                safelyRunUICode {
                                    completion(nil)
                                }
                            }
                        }
                    }
                case .restricted, .denied:
                    safelyRunUICode {
                        completion(nil)
                    }
                @unknown default:
                    break
                }
            }
        }
    }

    func getOutputOrientation(completion: @escaping (ExifOrientation) -> Void) {
        completion(orientationForCamera(activeCamera: activeCamera))
    }

    func setFlashEnabled(enabled: Bool) -> Bool {
        return true
    }

    func takePhoto(completion: @escaping (PhotoFromCamera?) -> Void) {
        guard let output = output,
            let connection = videoOutputConnection() else {
            return
        }

        if connection.isVideoOrientationSupported {
            connection.videoOrientation = avOrientationForCurrentDeviceOrientation()
        }

        let settings = AVCapturePhotoSettings()
        output.capturePhoto(with: settings, delegate: self)
        self.capturePhotoBlock = completion
    }

    func setCaptureSessionRunning(running: Bool) {
        if running {
            captureSession?.startRunning()
        } else {
            captureSession?.stopRunning()
        }
    }

    func focusOnPoint(focusPoint: CGPoint) -> Bool {
        guard let activeCamera = self.activeCamera,
            activeCamera.isFocusPointOfInterestSupported,
            activeCamera.isExposurePointOfInterestSupported else {
            return false
        }

        do {
            try activeCamera.lockForConfiguration()

            if activeCamera.isFocusPointOfInterestSupported {
                activeCamera.focusPointOfInterest = focusPoint
                activeCamera.focusMode = .continuousAutoFocus
            }

            if activeCamera.isExposurePointOfInterestSupported {
                activeCamera.exposurePointOfInterest = focusPoint
                activeCamera.exposureMode = .continuousAutoExposure
            }

            activeCamera.unlockForConfiguration()

            return true
        } catch {
            debugPrint("Couldn't focus camera: \(error)")
            return false
        }
    }

    func canToggleCamera(completion: @escaping (Bool) -> Void) {

    }

    func toggleCamera(completion: @escaping (ExifOrientation) -> Void) {

    }
}

extension GeneralCameraService: AVCapturePhotoCaptureDelegate {

    func photoOutput(_ captureOutput: AVCapturePhotoOutput,
                     didFinishProcessingPhoto photoSampleBuffer: CMSampleBuffer?,
                     previewPhoto previewPhotoSampleBuffer: CMSampleBuffer?,
                     resolvedSettings: AVCaptureResolvedPhotoSettings,
                     bracketSettings: AVCaptureBracketedStillImageSettings?,
                     error: Swift.Error?) {
        self.imageStorageService.save(sampleBuffer: photoSampleBuffer,
                                      callbackQueue: .main) { [weak self] path in
                                        guard let self = self else {
                                            return
                                        }

                                        guard let path = path else {
                                            self.capturePhotoBlock?(nil)
                                            return
                                        }

                                        self.capturePhotoBlock?(PhotoFromCamera(path: path))

        }
    }
}
