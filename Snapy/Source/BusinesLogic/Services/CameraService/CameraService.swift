//
//  CameraService.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import ImageSource

protocol CameraService {
    var isFlashAvailable: Bool { get }
    var isFlashEnabled: Bool { get }

    func getCaptureSession(completion: @escaping (AVCaptureSession?) -> Void)
    func getOutputOrientation(completion: @escaping (ExifOrientation) -> Void)

    // Returns a flag indicating whether changing flash mode was successful
    func setFlashEnabled(enabled: Bool) -> Bool

    func takePhoto(completion: @escaping (PhotoFromCamera?) -> Void)
    func setCaptureSessionRunning(running: Bool)

    func focusOnPoint(focusPoint: CGPoint) -> Bool

    func canToggleCamera(completion: @escaping (Bool) -> Void)
    func toggleCamera(completion: @escaping (_ newOutputOrientation: ExifOrientation) -> Void)
}

public struct PhotoFromCamera {
    let path: String
}
