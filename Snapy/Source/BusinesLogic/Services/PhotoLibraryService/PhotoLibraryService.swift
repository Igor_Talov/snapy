//
//  PhotoLibraryService.swift
//  Snapy
//
//  Created by Игорь Талов on 22/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol PhotoLibraryService {
    func getPhotos(completion: @escaping ([UIImage]) -> Void)
}
