//
//  Assets.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol Assets {
    var colors: Colors { get }
    var fonts: Fonts { get }
    var images: Images { get }
}
