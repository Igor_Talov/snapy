//
//  Colors.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol Colors {
    var primaryContent: UIColor { get }
    var primaryText: UIColor { get }

    var toolBackground: UIColor { get }
    var secondary: UIColor { get }

    var selected: UIColor { get }
    var unselected: UIColor { get }

    var defaultShadow: UIColor { get }
}
