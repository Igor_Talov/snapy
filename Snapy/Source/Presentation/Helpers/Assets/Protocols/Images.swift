//
//  Images.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol Images {
    var cameraIcon: UIImage { get }
    var messagesIcon: UIImage { get }
    var locationIcon: UIImage { get }
    var homeTabBarItemIcon: UIImage { get }

    // MARK: - StartScreen
    var backgroundImage: UIImage { get }
    var settings: UIImage { get }

    // MARK: - CameraView
    var gridIcon: UIImage { get }
    var flashOnIcon: UIImage { get }
    var cameraToggleIcon: UIImage { get }
    var closeIcon: UIImage { get }
    var doneIcon: UIImage { get }
    var temperature: UIImage { get }
}
