//
//  Fonts.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol Fonts {
//    var title1: UIFont { get }
    var title2: UIFont { get }
//    var title3: UIFont { get }
    var body: UIFont { get }
    var footnote: UIFont { get }
    var caption: UIFont { get }
}
