//
//  MainImages.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

final class MainImages: Images {
    private(set) lazy var cameraIcon: UIImage = {
        return #imageLiteral(resourceName: "cameraIcon.pdf")
    }()

    private(set) lazy var messagesIcon: UIImage = {
        return #imageLiteral(resourceName: "messages_icon.pdf")
    }()

    private(set) lazy var locationIcon: UIImage = {
        return #imageLiteral(resourceName: "location_icon.pdf")
    }()

    private(set) lazy var homeTabBarItemIcon: UIImage = {
        return #imageLiteral(resourceName: "Home.pdf")
    }()

    // MARK: - StartScreen
    private(set) lazy var backgroundImage: UIImage = {
        return #imageLiteral(resourceName: "StartScreenBackground.pdf")
    }()
    private(set) lazy var settings: UIImage = {
        return #imageLiteral(resourceName: "settings.pdf")
    }()

    // MARK: - CameraView
    private(set) lazy var gridIcon: UIImage = {
        return #imageLiteral(resourceName: "Grid.pdf")
    }()

    private(set) lazy var flashOnIcon: UIImage = {
        return #imageLiteral(resourceName: "Flash.pdf")
    }()

    private(set) lazy var cameraToggleIcon: UIImage = {
        return #imageLiteral(resourceName: "Repeat.pdf")
    }()

    private(set) lazy var closeIcon: UIImage = {
        return #imageLiteral(resourceName: "closeIcon.pdf")
    }()

    private(set) lazy var doneIcon: UIImage = {
        return #imageLiteral(resourceName: "doneIcon.pdf")
    }()

    private(set) lazy var temperature: UIImage = {
        return #imageLiteral(resourceName: "Temperature.pdf")
    }()

}
