//
//  MainAssets.swift
//  Snapy
//
//  Created by Игорь Талов on 09/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

final class MainAssets: Assets {
    // MARK: - Shared instances
    static let shared = MainAssets()

    // MARK: - Initialization
    init() {}

    // MAKR: - Assets
    let colors: Colors = MainColors()
    let fonts: Fonts = MainFonts()
    let images: Images = MainImages()
}
