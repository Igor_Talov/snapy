//
//  NibNameable.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol NibNameable {
    static var nibName: String { get }
}
