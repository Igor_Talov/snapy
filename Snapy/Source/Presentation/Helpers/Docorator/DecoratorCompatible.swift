//
//  DecoratorCompatible.swift
//  Snapy
//
//  Created by Игорь Талов on 22/12/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol DecoratorCompatible {
    associatedtype DecoratorCompatibleType
    var decorator: Decorator<DecoratorCompatibleType> { get }
}

extension DecoratorCompatible {
    var decorator: Decorator<Self> {
        return Decorator(object: self)
    }
}
