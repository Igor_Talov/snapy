//
//  Decorator.swift
//  Snapy
//
//  Created by Игорь Талов on 22/12/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

typealias Decoration<T> = (T) -> Void

struct Decorator<T> {
    let object: T

    func apply(_ decorations: Decoration<T>...) {
        decorations.forEach { $0(object) }
    }
}
