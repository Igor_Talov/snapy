//
//  UIView+NibInstantiable.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

extension UIView {
    static func initFromNib<T: UIView>() -> T {
        let nib = Bundle.main.loadNibNamed("\(nibName)", owner: self, options: nil)
        guard let view = nib?.first as? T else {
            fatalError("Couldn't find nib for \(self)")
        }

        return view
    }
}
