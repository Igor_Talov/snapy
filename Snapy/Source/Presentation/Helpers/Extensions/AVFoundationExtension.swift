//
//  AVFoundationExtension.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation

extension AVCaptureSession {
    func configure(configuration: () throws -> Void) throws {
        beginConfiguration()
        try configuration()
        commitConfiguration()
    }
}
