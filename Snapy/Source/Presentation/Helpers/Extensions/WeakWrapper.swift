//
//  WeakWrapper.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct WeakWrapper<T: AnyObject> {
    weak var value: T?

    init(value: T) {
        self.value = value
    }
}
