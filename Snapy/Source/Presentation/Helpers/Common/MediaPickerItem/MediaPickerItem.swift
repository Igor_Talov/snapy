//
//  MediaPickerItem.swift
//  Snapy
//
//  Created by Игорь Талов on 23/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import ImageSource

final class MediaPickerItem: Equatable {
    // MARK: - Public properties
    let image: ImageSource

    let identifier: String

    // MARK: - Initialization
    init(identifier: String = NSUUID().uuidString,
         image: ImageSource) {
        self.identifier = identifier
        self.image = image

    }

    static func == (item1: MediaPickerItem,
                    item2: MediaPickerItem) -> Bool {
        return item1.identifier == item2.identifier
    }
}
