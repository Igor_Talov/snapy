//
//  CameraOutputView.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import ImageSource

final class CameraOutputView: UIView {
    // MARK: - Private properties
    private var cameraView: CameraOutputRenderView?

    // MARK: - Public properties
    var orientation: ExifOrientation {
        didSet {
            cameraView?.orientation = orientation
        }
    }

    var onFrameDraw: (() -> Void)? {
        didSet {
            cameraView?.onFrameDraw = onFrameDraw
        }
    }

    // MARK: - Initialization
    init(captureSession: AVCaptureSession, outputOrientation: ExifOrientation) {
        self.orientation = outputOrientation
        super.init(frame: .zero)

        let eaglContext: EAGLContext? = EAGLContext(api: .openGLES2)

        let glkView = eaglContext.flatMap { eaglContext in
            CameraOutputGLKView(
                captureSession: captureSession,
                outputOrientation: outputOrientation,
                context: eaglContext
            )
        }

        if let glkView = glkView {
            cameraView = glkView
            addSubview(glkView)
        }
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func layoutSubviews() {
        super.layoutSubviews()
        cameraView?.frame = bounds
    }

}
