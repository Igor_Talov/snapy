//
//  CaptureSessionPreviewService.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import ImageSource

protocol CaptureSessionPreviewService: class {
    func startStreamPreview(to handler: CameraOutputHandler) -> DispatchQueue
}
