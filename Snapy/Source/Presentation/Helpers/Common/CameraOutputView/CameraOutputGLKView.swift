//
//  CameraOutputGLKView.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import GLKit
import ImageSource

final class CameraOutputGLKView: GLKView, CameraOutputRenderView, CameraOutputHandler {

    // MARK: - Private properties
    private let ciContext: CIContext
    private let previewService: CaptureSessionPreviewService
    private var hasWindow = false
    private var mainQueue = DispatchQueue.main

    // MARK: - CameraOutputRenderView
    var orientation: ExifOrientation
    var onFrameDraw: (() -> Void)?

    // MARK: - CameraOutputHandler
    var imageBuffer: CVImageBuffer? {
        didSet {
            if hasWindow {
                display()
            }
        }
    }

    // MARK: - Initialization
    init(captureSession: AVCaptureSession, outputOrientation: ExifOrientation, context: EAGLContext) {
        self.orientation = outputOrientation
        ciContext = CIContext(eaglContext: context, options: [CIContextOption.workingColorSpace: NSNull()])
        previewService = GeneralCaptureSessionPreviewService(captureSession: captureSession,
                                                             isMirrored: outputOrientation.isMirrored)
        super.init(frame: .zero, context: context)
        clipsToBounds = true
        enableSetNeedsDisplay = false

        mainQueue = previewService.startStreamPreview(to: self)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func didMoveToWindow() {
        super.didMoveToWindow()
        let hasWindow = (window != nil)

        mainQueue.async { [weak self] in
            self?.hasWindow = hasWindow
        }
    }
}

extension CameraOutputGLKView {
    override func draw(_ rect: CGRect) {
        guard let imageBuffer = imageBuffer else { return }

        let image = CIImage(cvPixelBuffer: imageBuffer)

        ciContext.draw(
            image,
            in: drawableBounds(for: rect),
            from: sourceRect(of: image, targeting: rect)
        )

        onFrameDraw?()
    }
}

private extension CameraOutputGLKView {
    func drawableBounds(for rect: CGRect) -> CGRect {
        let screenScale = UIScreen.main.nativeScale

        var drawableBounds = rect
        drawableBounds.size.width *= screenScale
        drawableBounds.size.height *= screenScale

        return drawableBounds
    }

    func sourceRect(of image: CIImage, targeting rect: CGRect) -> CGRect {
        guard image.extent.width > 0, image.extent.height > 0, rect.width > 0, rect.height > 0 else {
            return .zero
        }

        let sourceExtent = image.extent

        let sourceAspect = sourceExtent.size.width / sourceExtent.size.height
        let previewAspect = rect.size.width  / rect.size.height

        var drawRect = sourceExtent

        if sourceAspect > previewAspect {
            drawRect.origin.x += (drawRect.size.width - drawRect.size.height * previewAspect) / 2
            drawRect.size.width = drawRect.size.height * previewAspect
        } else {
            drawRect.origin.y += (drawRect.size.height - drawRect.size.width / previewAspect) / 2
            drawRect.size.height = drawRect.size.width / previewAspect
        }

        return drawRect
    }
}
