//
//  GeneralCaptureSessionPreviewService.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import Foundation
import ImageSource

@objc
public protocol CameraOutputHandler: class {
    var imageBuffer: CVImageBuffer? { get set }
}

final class GeneralCaptureSessionPreviewService: NSObject {
    // MARK: - Private properties
    private var handlers = [WeakWrapper<CameraOutputHandler>]()
    private let workQueue = DispatchQueue(label: "com.SealDev.generalCaptureSessionPreviewService")

    init(captureSession: AVCaptureSession, isMirrored: Bool) {
        super.init()
        setupCaptureVideoDataOutput(for: captureSession, isMirrored: isMirrored)
    }
}

private extension GeneralCaptureSessionPreviewService {
    func setupCaptureVideoDataOutput(for session: AVCaptureSession, isMirrored: Bool) {
        let captureOutput = AVCaptureVideoDataOutput()
        captureOutput.alwaysDiscardsLateVideoFrames = true
        captureOutput.setSampleBufferDelegate(self, queue: workQueue)

        do {
            try session.configure {
                if session.canAddOutput(captureOutput) {
                    session.addOutput(captureOutput)
                }

                for connection in captureOutput.connections {
                    if connection.isVideoMirroringSupported {
                        connection.videoOrientation = .portrait
                        connection.isVideoMirrored = isMirrored
                    }
                }
            }
        } catch {
            assertionFailure("can't configure AVCaptureSession")
        }
    }
}

extension GeneralCaptureSessionPreviewService: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection) {
        if let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) {
            handlers.forEach { wrapper in
                wrapper.value?.imageBuffer = imageBuffer
            }
        }
    }
}

extension GeneralCaptureSessionPreviewService: CaptureSessionPreviewService {
    func startStreamPreview(to handler: CameraOutputHandler) -> DispatchQueue {
        workQueue.async { [weak self] in
            guard let self = self else {
                return
            }
            self.handlers.append(WeakWrapper(value: handler))
        }
        return workQueue
    }
}
