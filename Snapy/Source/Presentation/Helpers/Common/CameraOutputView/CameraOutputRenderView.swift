//
//  CameraOutputRenderView.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import ImageSource

protocol CameraOutputRenderView: class {
    var frame: CGRect { get set }
    var orientation: ExifOrientation { get set }
    var onFrameDraw: (() -> Void)? { get set }
}
