//
//  SettingViewCell.swift
//  Snapy
//
//  Created by Игорь Талов on 02/02/2020.
//  Copyright © 2020 Игорь Талов. All rights reserved.
//

import UIKit

class SettingViewCell: UITableViewCell {

    // MARK: - Outlets
    @IBOutlet private weak var titleLabel: UILabel?

    // MARK: - Public properties
    var viewModel: String? {
        didSet {
            displayViewModelOnView()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension SettingViewCell {
    func displayViewModelOnView() {
        safelyRunUICode {
            self.titleLabel?.text = self.viewModel ?? ""
        }
    }
}
