//
//  SettingsViewIO.swift
//  Snapy
//
//  Created by Игорь Талов on 02/02/2020.
//  Copyright © 2020 Игорь Талов. All rights reserved.
//

protocol SettingsViewInput: AnyObject {
    func displaySettings(viewModels: [String])
}

protocol SettingsViewOutput: AnyObject {
    func didChooseSettings(settings: String)
}
