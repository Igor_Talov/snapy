//
//  SettingsViewController.swift
//  Snapy
//
//  Created by Игорь Талов on 02/02/2020.
//  Copyright © 2020 Игорь Талов. All rights reserved.
//

import UIKit

class SettingsViewController: UIViewController {

    // MARK: - Public properties
    var viewModels: [String] = []

    // MARK: - Outlets
    @IBOutlet private weak var tableView: UITableView?

    override func viewDidLoad() {
        super.viewDidLoad()
        setupTableView()
    }
}

extension SettingsViewController: UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return viewModels.count
    }

    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "SettingCell", for: indexPath)
        cell.textLabel?.text = viewModels[indexPath.row]
        return cell
    }
}

extension SettingsViewController: UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        tableView.deselectRow(at: indexPath, animated: true)
    }
}

private extension SettingsViewController {
    func setupTableView() {
        tableView?.dataSource = self
        tableView?.delegate = self
    }
}
