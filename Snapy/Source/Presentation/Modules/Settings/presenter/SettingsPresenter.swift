//
//  SettingsPresenter.swift
//  Snapy
//
//  Created by Игорь Талов on 02/02/2020.
//  Copyright © 2020 Игорь Талов. All rights reserved.
//

import Foundation

class SettingsPresenter {
    // MARK: - Private properties
    private weak var view: SettingsViewInput?

    // MARK: - Initialization
    init(view: SettingsViewInput) {
        self.view = view
    }
}
