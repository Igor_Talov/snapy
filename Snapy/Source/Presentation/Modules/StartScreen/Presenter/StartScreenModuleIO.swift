//
//  StartScreenModuleIO.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - StartScreenModuleInput
protocol StartScreenModuleInput: class {}

// MARK: - StartScreenModuleOutput
protocol StartScreenModuleOutput: class {
    func startScreenModuleDidCameraRequest(_ module: StartScreenModuleInput)
    func startScreenModuleDidPhotoLibrarryRequest(_ module: StartScreenModuleInput)
}
