//
//  StartScreenPresenter.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class StartScreenPresenter {
    // MARK: - Private properties
    private weak var view: StartScreenViewInput?

    // MARK: - Public properties
    weak var output: StartScreenModuleOutput?

    // MARK: - Initialization
    init(view: StartScreenViewInput) {
        self.view = view
    }
}

private extension StartScreenPresenter {
    func displayButtons() {
        let cameraButtonViewModel = ButtonViewModel(title: NSLocalizedString("STARTSCREEN_TAKE_PHOTO_TITLE", comment: ""),
                                                    action: nil)
        let photoLibraryButtonViewModel = ButtonViewModel(title: NSLocalizedString("STARTSCREEN_PHOTO_LIBRARY", comment: ""),
                                                          action: nil)
        view?.displayCameraButton(viewModel: cameraButtonViewModel)
        view?.displayPhotoLibraryButton(viewModel: photoLibraryButtonViewModel)
    }
}

extension StartScreenPresenter: StartScreenViewOutput {
    func didTapSettingsButton() {
        
    }

    func viewDidiLoad() {
        displayButtons()
    }

    func didTapCameraButton() {
        output?.startScreenModuleDidCameraRequest(self)
    }

    func didTapPhotoLibraryButton() {
        output?.startScreenModuleDidPhotoLibrarryRequest(self)
    }
}

extension StartScreenPresenter: StartScreenModuleInput {}
