//
//  ButtonViewModels.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct ButtonViewModel {
    let title: String
    let action: (() -> Void)?
}
