//
//  StartViewIO.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - StartScreenViewInput
protocol StartScreenViewInput: class {
    func displayCameraButton(viewModel: ButtonViewModel?)
    func displayPhotoLibraryButton(viewModel: ButtonViewModel?)
}

// MARK: - StartScreenViewOutput
protocol StartScreenViewOutput: class {
    func viewDidiLoad()

    func didTapCameraButton()
    func didTapPhotoLibraryButton()
    func didTapSettingsButton()
}
