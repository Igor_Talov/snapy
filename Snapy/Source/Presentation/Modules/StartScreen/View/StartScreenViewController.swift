//
//  StartScreenViewController.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class StartScreenViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Public properties
    var output: StartScreenViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var cameraButton: UIButton? {
        didSet {
            cameraButton?.isHidden = true
        }
    }
    @IBOutlet private weak var photoLibraryButton: UIButton? {
        didSet {
            photoLibraryButton?.isHidden = true
        }
    }
    @IBOutlet private weak var settingsButton: UIButton?
    @IBOutlet private weak var backgroundImageView: UIImageView? {
        didSet {
            backgroundImageView?.image = style.assets.images.backgroundImage
        }
    }

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)), bundle: nil)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidiLoad()
    }
}

extension StartScreenViewController: StartScreenViewInput {
    func displayCameraButton(viewModel: ButtonViewModel?) {
        safelyRunUICode {
            self.cameraButton?.isHidden = false
            self.cameraButton?.titleLabel?.font = self.style.assets.fonts.title2
            self.cameraButton?.setTitle(viewModel?.title,
                                        for: .normal)
        }
    }

    func displayPhotoLibraryButton(viewModel: ButtonViewModel?) {
        safelyRunUICode {
            self.photoLibraryButton?.isHidden = false
            self.photoLibraryButton?.titleLabel?.font = self.style.assets.fonts.title2
            self.photoLibraryButton?.setTitle(viewModel?.title,
                                              for: .normal)
        }
    }
}

private extension StartScreenViewController {
    @IBAction func didTapCameraButton(_ sender: UIButton) {
        output?.didTapCameraButton()
    }

    @IBAction func didTapPhotoLibraryButton(_ sender: UIButton) {
        output?.didTapPhotoLibraryButton()
    }
    @IBAction func didTapSettingsButton(_ sender: UIButton) {
        output?.didTapSettingsButton()
    }
}
