//
//  GeneralStartScreenModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralStartScreenModuleFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralStartScreenModuleFactory: StartScreenModuleFactory {
    func makeStartScreenModule(output: StartScreenModuleOutput) -> ModuleInterface<StartScreenModuleInput> {
        let builder = StartScreenModuleBuilder(style: style)
        return builder.buildModule(with: output)
    }
}
