//
//  StartScreenModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol StartScreenModuleFactory {
    func makeStartScreenModule(output: StartScreenModuleOutput) -> ModuleInterface<StartScreenModuleInput>
}
