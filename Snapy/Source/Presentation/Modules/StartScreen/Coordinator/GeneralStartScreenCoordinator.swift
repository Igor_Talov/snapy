//
//  GeneralStartScreenCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralStartScreenCoordinator: NSObject, StartsScreenCoordinator {
    // MARK: - Private properties
    private let style: Style
    private let moduleFactory: StartScreenModuleFactory
    private let coordinatorfactory: StartScreenCoordinatorFactory
    private(set) var isStarted = false
    private var rootNavigationController: UINavigationController?

    private var imagePicker: UIImagePickerController?

    private var childCoordinators: [Coordinator] = []

    // MARK: - Initialization
    init(style: Style,
         moduleFactory: StartScreenModuleFactory,
         coordinatorfactory: StartScreenCoordinatorFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
        self.coordinatorfactory = coordinatorfactory
    }

    func start(on rootController: UINavigationController) {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return
        }
        isStarted = true
        rootNavigationController = rootController

        let module = moduleFactory.makeStartScreenModule(output: self)
        let controller = module.rootViewController
        rootController.setViewControllers([controller], animated: false)
    }
}

extension GeneralStartScreenCoordinator: StartScreenModuleOutput {
    func startScreenModuleDidCameraRequest(_ module: StartScreenModuleInput) {
        let coordinator = coordinatorfactory.makeCameraCoordinator()
        childCoordinators.append(coordinator)
        coordinator.onDismiss = { [weak self] in
            guard let self = self else {
                return
            }
            self.dismissCamera()
        }
        let cameraController = coordinator.start()
        cameraController.modalTransitionStyle = .coverVertical
        cameraController.modalPresentationStyle = .overFullScreen
        safelyRunUICode { [weak self] in
            guard let self = self else { return }
            self.rootNavigationController?.present(cameraController,
                                                   animated: true,
                                                   completion: nil)
        }
    }

    func startScreenModuleDidPhotoLibrarryRequest(_ module: StartScreenModuleInput) {
        let pickerViewController = UIImagePickerController()
        pickerViewController.allowsEditing = true
        pickerViewController.sourceType = .photoLibrary
        pickerViewController.delegate = self

        rootNavigationController?
            .topViewController?
            .present(pickerViewController,
                     animated: true,
                     completion: nil)
    }
}

extension GeneralStartScreenCoordinator: UINavigationControllerDelegate { }

extension GeneralStartScreenCoordinator: UIImagePickerControllerDelegate {
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        rootNavigationController?.topViewController?.dismiss(animated: true, completion: nil)
    }

    func imagePickerController(_ picker: UIImagePickerController,
                               didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey: Any]) {
        if let pickedImage = info[UIImagePickerController.InfoKey.originalImage] as? UIImage {
            if let ciimage = CIImage(image: pickedImage) {
                rootNavigationController?.topViewController?.dismiss(animated: true, completion: nil)
                showEditorScreen(with: ciimage)
            }
        }
    }
}

private extension GeneralStartScreenCoordinator {
    func dismissCamera() {
        childCoordinators.removeAll()
        self.rootNavigationController?.dismiss(animated: true,
                                               completion: nil)
    }

    func showEditorScreen(with sourceImage: CIImage) {
        let coordinator = coordinatorfactory.makeEditorCoordinator()
        childCoordinators.append(coordinator)
        let editorData = EditorData(ciimage: sourceImage)
        let editorController = coordinator.start(with: editorData)
        rootNavigationController?.pushViewController(editorController, animated: true)
    }
}
