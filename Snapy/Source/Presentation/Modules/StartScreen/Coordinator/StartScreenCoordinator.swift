//
//  StartScreenCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol StartsScreenCoordinator: Coordinator {
    func start(on rootController: UINavigationController)
}
