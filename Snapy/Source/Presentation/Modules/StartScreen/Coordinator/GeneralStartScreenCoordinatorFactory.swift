//
//  GeneralStartScreenCoordinatorFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralStartScreenCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralStartScreenCoordinatorFactory: StartScreenCoordinatorFactory {
    func makeEditorCoordinator() -> EditorCoordinator {
        let moduleFactory = GeneralEditorModuleFactory(style: style)
        let coordinator = GeneralEditorCoordinator(style: style,
                                                   moduleFactory: moduleFactory)
        return coordinator
    }

    func makeCameraCoordinator() -> CameraCoordinator {
        let moduleFactory = GeneralCameraModuleFactory(style: style)
        let coordinator = GeneralCameraCoordinator(style: style,
                                                   moduleFactory: moduleFactory)
        return coordinator
    }
}
