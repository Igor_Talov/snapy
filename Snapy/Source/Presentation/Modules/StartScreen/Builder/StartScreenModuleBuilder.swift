//
//  StartScreenModuleBuilder.swift
//  Snapy
//
//  Created by Игорь Талов on 09/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class StartScreenModuleBuilder {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func buildModule(with output: StartScreenModuleOutput) -> ModuleInterface<StartScreenModuleInput> {
        let view = makeView()
        let presenter = makePresenter(view: view)
        presenter.output = output
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension StartScreenModuleBuilder {
    func makeView() -> StartScreenViewController {
        return StartScreenViewController(style: style)
    }

    func makePresenter(view: StartScreenViewInput) -> StartScreenPresenter {
        return StartScreenPresenter(view: view)
    }
}
