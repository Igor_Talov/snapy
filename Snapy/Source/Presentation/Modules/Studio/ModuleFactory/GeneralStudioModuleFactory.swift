//
//  GeneralStudioModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralStudioModuleFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralStudioModuleFactory: StudioModuleFactory {
    func makeStudioModule(output: StudioModuleOutput) -> ModuleInterface<StudioModuleInput> {
        let moduleBuilder = StudioModuleBuilder(style: style)
        return moduleBuilder.buildModule(with: output)
    }
}
