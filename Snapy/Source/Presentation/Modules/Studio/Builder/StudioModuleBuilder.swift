//
//  StudioModuleBuilder.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class StudioModuleBuilder {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func buildModule(with output: StudioModuleOutput) -> ModuleInterface<StudioModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)
        presenter.output = output
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension StudioModuleBuilder {
    func makeView() -> StudioViewController {
        let controller = StudioViewController(style: style)
        return controller
    }

    func makeInteractor() -> StudioInteractor {
        let imageStorageService = GeneralImageStorageService()
        let interactor = StudioInteractor(imageStorageService: imageStorageService)
        return interactor
    }
    func makePresenter(view: StudioViewInput,
                       interactor: StudioInteractorInput) -> StudioPresenter {
        let presenter = StudioPresenter(view: view,
                                        interactor: interactor)
        return presenter
    }
}
