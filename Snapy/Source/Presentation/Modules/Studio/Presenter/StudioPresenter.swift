//
//  StudioPresenter.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import ImageSource

class StudioPresenter {
    // MARK: - Private properties
    private weak var view: StudioViewInput?
    private let interactor: StudioInteractorInput

    // MARK: - Public properties
    weak var output: StudioModuleOutput?

    // MARK: - Initialization
    init(view: StudioViewInput,
         interactor: StudioInteractorInput) {
        self.view = view
        self.interactor = interactor
    }
}

extension StudioPresenter: StudioModuleInput {}

extension StudioPresenter: StudioViewOutput {
    func viewDidLoad() {
        preparePlaceholder()
        interactor.fetchPhotoItems { [weak self] items in
            guard let self = self else {
                return
            }
            let mediaPickerItems = items.map { self.makeMediaPickerItem(photo: $0) }
            let viewModels = mediaPickerItems.map { self.makeViewModel(item: $0) }
            self.view?.displayItems(items: viewModels)
        }
    }

    func didTapCameraButton() {
        output?.studioModuleDidCameraRequest(self)
    }
}

private extension StudioPresenter {
    func makePlaceholderViewModel() -> PlaceholderViewModel {
        let title = NSLocalizedString("STUDIO_PLACEHOLDER_TITLE", comment: "")
        return PlaceholderViewModel(title: title)
    }

    func makeViewModel(item: MediaPickerItem) -> StudioViewModel {
        let viewModel = StudioViewModel(imageSource: item.image)
        return viewModel
    }

    func preparePlaceholder() {
        let viewModel = makePlaceholderViewModel()
        view?.displayPlaceholder(viewModel: viewModel)
    }

    func makeMediaPickerItem(photo: PhotoFromCamera) -> MediaPickerItem {
        let imageSource = LocalImageSource(path: photo.path)
        let mediaPickerItem = MediaPickerItem(image: imageSource)
        return mediaPickerItem
    }
}
