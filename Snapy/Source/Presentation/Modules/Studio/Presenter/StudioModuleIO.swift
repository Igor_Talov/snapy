//
//  StudioModuleIO.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - StudioModuleInput
protocol StudioModuleInput: class {
}

// MARK: - StudioModuleOutput
protocol StudioModuleOutput: class {
    func studioModuleDidCameraRequest(_ module: StudioModuleInput)
}
