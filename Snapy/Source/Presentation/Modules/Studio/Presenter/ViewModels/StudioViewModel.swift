//
//  StudioViewModel.swift
//  Snapy
//
//  Created by Игорь Талов on 24/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation
import ImageSource

struct StudioViewModel {
    let imageSource: ImageSource
    let isEdited: Bool = false
}
