//
//  StudioViewIO.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

// MARK: - StudioViewInput
protocol StudioViewInput: class {
    // display photos
    func displayPlaceholder(viewModel: PlaceholderViewModel?)
    func displayItems(items: [StudioViewModel])
    func hidePlaceholder()
}

// MARK: - StudioViewOutput
protocol StudioViewOutput: class {
    func viewDidLoad()

    func didTapCameraButton()
}
