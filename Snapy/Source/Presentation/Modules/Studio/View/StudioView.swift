//
//  StudioView.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let cellReuseIdentifier = "StudioViewCell"
}

class StudioViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style
    private var items: [StudioViewModel] = []

    // MARK: - Public properties
    var output: StudioViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var placeholderView: StudioPlaceholderView? {
        didSet {
            placeholderView?.isHidden = true
        }
    }

    @IBOutlet private weak var collectionView: UICollectionView? {
        didSet {
            collectionView?.isHidden = true
        }
    }

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)),
                   bundle: nil)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    // MARK: - Lifecycle
    override func viewDidLoad() {
        super.viewDidLoad()
        setupBarButtonItems()
        setupCollectionView()
        output?.viewDidLoad()
    }
}

extension StudioViewController: UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }

    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: Constants.cellReuseIdentifier,
                                                      for: indexPath) as? StudioCollectionViewCell

        let viewModel = items[indexPath.row]
        cell?.style = style
        cell?.viewModel = viewModel
        return cell ?? UICollectionViewCell()
    }
}

extension StudioViewController: UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let sideSize = view.bounds.width / 2
        return CGSize(width: sideSize, height: sideSize)
    }
}

private extension StudioViewController {
    func setupBarButtonItems() {
        let image = style.assets.images.cameraIcon
        let cameraBarButtonItem = UIBarButtonItem(image: image,
                                                  style: .plain,
                                                  target: self,
                                                  action: #selector(didTapCameraButton(_:)))
        self.navigationItem.leftBarButtonItem = cameraBarButtonItem
    }

    func setupCollectionView() {
        collectionView?.register(UINib(nibName: StudioCollectionViewCell.nibName,
                                       bundle: nil),
                                 forCellWithReuseIdentifier: Constants.cellReuseIdentifier)
        collectionView?.dataSource = self
        collectionView?.delegate = self
    }
}

private extension StudioViewController {
    @objc
    func didTapCameraButton(_ sender: UIBarButtonItem) {
        output?.didTapCameraButton()
    }
}

extension StudioViewController: StudioViewInput {
    func displayItems(items: [StudioViewModel]) {
        safelyRunUICode {
            self.collectionView?.isHidden = items.isEmpty
            self.items = items
            self.collectionView?.reloadData()
        }
    }

    func displayPlaceholder(viewModel: PlaceholderViewModel?) {
        safelyRunUICode {
            self.placeholderView?.isHidden = viewModel == nil
            self.placeholderView?.style = self.style
            self.placeholderView?.viewModel = viewModel
        }
    }

    func hidePlaceholder() {
        safelyRunUICode {
            self.placeholderView?.isHidden = true
        }
    }
}
