//
//  StudioPlaceholderView.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class StudioPlaceholderView: UIView {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: PlaceholderViewModel? {
        didSet {
            bindViewModel()
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var titleLabel: UILabel? {
        didSet {
            updateTitleLabelStyle()
        }
    }

    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
        updateElementsStyle()
    }
}

private extension StudioPlaceholderView {
    func updateElementsStyle() {
        updateTitleLabelStyle()
    }

    func updateTitleLabelStyle() {
        titleLabel?.font = style?.assets.fonts.body
        titleLabel?.textColor = style?.assets.colors.primaryText
    }

    func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }
        titleLabel?.text = viewModel.title
    }
}
