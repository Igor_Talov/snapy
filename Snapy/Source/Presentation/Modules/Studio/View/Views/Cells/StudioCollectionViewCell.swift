//
//  StudioCollectionViewCell.swift
//  Snapy
//
//  Created by Игорь Талов on 24/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import ImageSource
import UIKit

class StudioCollectionViewCell: UICollectionViewCell {
    // MARK: - Private properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: StudioViewModel? {
        didSet {
            bindViewModel(viewModel: viewModel)
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var imageView: UIImageView?

    required init?(coder: NSCoder) {
        super.init(coder: coder)
        updateElementsStyle()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension StudioCollectionViewCell {
    func updateElementsStyle() {

    }

    func bindViewModel(viewModel: StudioViewModel?) {
        guard let viewModel = viewModel else {
            return
        }
        self.imageView?.setImage(fromSource: viewModel.imageSource)
    }
}
