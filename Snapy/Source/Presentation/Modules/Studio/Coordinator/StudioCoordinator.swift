//
//  StudioCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol StudioCoordinator: Coordinator {

    var isStarted: Bool { get }
    var rootViewController: UINavigationController? { get }

    var onCameraRequest: (() -> Void)? { get set }
    var onPhotoLibraryRequest: (() -> Void)? { get set }

    func start() -> UIViewController
}
