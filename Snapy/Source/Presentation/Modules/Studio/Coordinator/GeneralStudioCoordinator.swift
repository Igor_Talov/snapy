//
//  GeneralStudioCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralStudioCoordinator: StudioCoordinator {
    // MARK: - Private properties
    private let style: Style
    private(set) var isStarted = false
    private let moduleFactory: StudioModuleFactory
    private let coordinatorFactory: StudioCoordinatorFactory

    private var childCoordinators: [Coordinator] = []

    private(set) var rootViewController: UINavigationController?

    init(style: Style,
         moduleFactory: StudioModuleFactory,
         coordinatorFactory: StudioCoordinatorFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
        self.coordinatorFactory = coordinatorFactory
    }

    var onCameraRequest: (() -> Void)?
    var onPhotoLibraryRequest: (() -> Void)?

    func start() -> UIViewController {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return UIViewController()
        }
        isStarted = true

        let module = moduleFactory.makeStudioModule(output: self)
        let controller = module.rootViewController
        let navigationController = UINavigationController(rootViewController: controller)
        navigationController.navigationBar.tintColor = style.assets.colors.toolBackground
        rootViewController = navigationController
        return navigationController
    }
}

extension GeneralStudioCoordinator: StudioModuleOutput {
    func studioModuleDidCameraRequest(_ module: StudioModuleInput) {
        let coordinator = coordinatorFactory.makeCameraCoordinator()
        coordinator.onDismiss = { [weak self] in
            guard let self = self else {
                return
            }
            self.dismissCamera()
        }
        childCoordinators.append(coordinator)
        let controller = coordinator.start()
        controller.modalTransitionStyle = .coverVertical
        controller.modalPresentationStyle = .overFullScreen
        safelyRunUICode {
            self.rootViewController?.present(controller,
                                             animated: true,
                                             completion: nil)
        }
    }
}

private extension GeneralStudioCoordinator {
    func dismissCamera() {
        childCoordinators.removeAll()
        self.rootViewController?.dismiss(animated: true,
                                         completion: nil)
    }
}
