//
//  GeneralStudioCoordinatorFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 24/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralStudioCoordinatorFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralStudioCoordinatorFactory: StudioCoordinatorFactory {
    func makeCameraCoordinator() -> CameraCoordinator {
         let moduleFactory = GeneralCameraModuleFactory(style: style)
         let coordinator = GeneralCameraCoordinator(style: style,
                                                    moduleFactory: moduleFactory)
         return coordinator
    }
}
