//
//  StudioInteractorIO.swift
//  Snapy
//
//  Created by Игорь Талов on 24/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol StudioInteractorInput: class {
    func fetchPhotoItems(completion: @escaping ([PhotoFromCamera]) -> Void)
}

protocol StudioInteractorOutput: class { }
