//
//  StudioInteractor.swift
//  Snapy
//
//  Created by Игорь Талов on 24/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class StudioInteractor {
    // MARK: - Private properties
    private let imageStorageService: ImageStorageService

    var output: StudioInteractorOutput?

    // MARK: - Initialization
    init(imageStorageService: ImageStorageService) {
        self.imageStorageService = imageStorageService
    }
}

extension StudioInteractor: StudioInteractorInput {
    func fetchPhotoItems(completion: @escaping ([PhotoFromCamera]) -> Void) {
        imageStorageService.getPhotos { items in
            guard let items = items else {
                return
            }
            completion(items)
        }
    }
}
