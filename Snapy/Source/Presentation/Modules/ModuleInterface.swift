//
//  ModuleInterface.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol ModuleView: class {}

struct Module<ModuleInput> {
    let input: ModuleInput
    let view: ModuleView
}

struct ModuleInterface<ModuleInput> {
    let input: ModuleInput
    let rootViewController: UIViewController
}
