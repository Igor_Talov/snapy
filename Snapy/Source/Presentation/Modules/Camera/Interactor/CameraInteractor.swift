//
//  CameraInteractor.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import ImageSource

class CameraInteractor {
    // MARK: - Private properties
    private let cameraService: CameraService

    // MARK: - Initialization
    init(cameraService: CameraService) {
        self.cameraService = cameraService
    }
}

extension CameraInteractor: CameraInteractorInput {
    func focusCameraOnPoint(focusPoint: CGPoint) -> Bool {
        return cameraService.focusOnPoint(focusPoint: focusPoint)
    }

    func takePhoto(completion: @escaping (MediaPickerItem?) -> Void) {
        cameraService.takePhoto { [weak self] photo in
            guard let self = self else {
                return
            }
            self.mediaPickerItem(photo?.path, completion: completion)
        }
    }

    func getOutputParameters(completion: @escaping (CameraOutputParameters?) -> Void) {
        cameraService.getCaptureSession { [cameraService] session in
            guard let session = session else {
                assertionFailure("session is nil")
                return
            }
            cameraService.getOutputOrientation { orientation in
                completion(CameraOutputParameters(captureSession: session,
                                                  orientation: orientation,
                                                  isMetalEnabled: orientation.isMirrored))
            }
        }
    }
}

private extension CameraInteractor {
    func mediaPickerItem(_ photoPath: String?, completion: @escaping (MediaPickerItem?) -> Void) {
        guard let path = photoPath else {
            return
        }

        let imageSource = LocalImageSource(path: path)
        let mediaPickerItem = MediaPickerItem(image: imageSource)
        completion(mediaPickerItem)
    }
}
