//
//  CameraInteractorIO.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import AVFoundation
import ImageSource

protocol CameraInteractorInput: class {
    func getOutputParameters(completion: @escaping (CameraOutputParameters?) -> Void)
    func takePhoto(completion: @escaping (MediaPickerItem?) -> Void)
    func focusCameraOnPoint(focusPoint: CGPoint) -> Bool
}

struct CameraOutputParameters {
    let captureSession: AVCaptureSession
    let orientation: ExifOrientation
    let isMetalEnabled: Bool
}
