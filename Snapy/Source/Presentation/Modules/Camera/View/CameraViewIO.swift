//
//  CameraViewIO.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreGraphics
import Foundation

// MARK: - CameraViewInput
protocol CameraViewInput: class {
    func setCameraOutputParameters(parameters: CameraOutputParameters)
    func setEVParameters(parameters: ParameterViewModel)
    func setISOParameters(parameters: ParameterViewModel)
    func setShutterSpeedParameters(parameters: ParameterViewModel)

    // MARK: - Display
    func displayFocus(onPoint focusPoint: CGPoint)
    func displayPickerImage(mediaPickerItem: MediaPickerItem)
    func displayFunctionButtons(viewModels: [FunctionViewModel])
}

protocol CameraViewOutput: class {
    func viewDidLoad()

    // MARK: - Actions
    func didTapCloseButton()
    func didTapFocus(focusPoint: CGPoint, touchPoint: CGPoint)
}
