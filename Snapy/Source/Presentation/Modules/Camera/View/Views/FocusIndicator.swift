//
//  FocusIndicator.swift
//  Snapy
//
//  Created by Игорь Талов on 18/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let radius: CGFloat = 30.0
    static let sideLenght: CGFloat = 80.0
    static let cornerRadius: CGFloat = 10.0

    // MARK: - Animation Constants
    static let duration: TimeInterval = 0.6
    static let opacityAnimationFromValue: CGFloat = 0
    static let scaleAnimationFromValue: CGFloat = 0.8
}

final class FocusIndicator: CALayer {
    // MARK: - Private properties
    private let shapeLayer = CAShapeLayer()
    private let appStyle: Style

    init(style: Style) {
        self.appStyle = style
        super.init()
        let rect = CGRect(x: 0,
                          y: 0,
                          width: Constants.sideLenght,
                          height: Constants.sideLenght)

        let path = UIBezierPath(roundedRect: rect,
                                cornerRadius: Constants.cornerRadius)

        shapeLayer.path = path.cgPath
        shapeLayer.fillColor = UIColor.clear.cgColor
        shapeLayer.strokeColor = style.assets.colors.selected.cgColor

        addSublayer(shapeLayer)

        bounds = CGRect(x: 0,
                        y: 0,
                        width: 2 * Constants.sideLenght,
                        height: 2 * Constants.sideLenght)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    func animate(in superlayer: CALayer, focusPoint: CGPoint) {
        CATransaction.begin()
        CATransaction.setDisableActions(true)
        position = focusPoint

        superlayer.addSublayer(self)
        CATransaction.setCompletionBlock {
            self.removeFromSuperlayer()
        }

        self.add(FocusIndicatorScaleAnimation(), forKey: nil)
        self.add(FocusIndicatorOpacityAnimation(), forKey: nil)
        opacity = 0

        CATransaction.commit()
    }

    func hide() {
        removeAllAnimations()
        removeFromSuperlayer()
    }
}

final class FocusIndicatorScaleAnimation: CABasicAnimation {
    override init() {
        super.init()
        keyPath = "transform.scale"
        fromValue = Constants.scaleAnimationFromValue
        toValue = 1.0
        duration = Constants.duration
        autoreverses = true
        isRemovedOnCompletion = false
        timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

final class FocusIndicatorOpacityAnimation: CABasicAnimation {
    override init() {
        super.init()
        keyPath = "opacity"
        fromValue = Constants.opacityAnimationFromValue
        toValue = 1.0
        duration = Constants.duration
        autoreverses = true
        isRemovedOnCompletion = false
        timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeIn)
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}
