//
//  ShutterButton.swift
//  Snapy
//
//  Created by Игорь Талов on 25/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let borderWidth: CGFloat = 4
}

class ShutterButton: UIButton {
    // MARK: - Private properties
    private let style: Style

    init(style: Style,
         frame: CGRect) {
        self.style = style
        super.init(frame: frame)
        setupDecorations()
    }

    override var isHighlighted: Bool {
        didSet {
            if isHighlighted {
                animateDown()
            } else {
                animateUp()
            }
        }
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension ShutterButton {
    // MARK: - Decorate
    // TODO: Make decorate generic protocol 🤔
    func setupDecorations() {
        backgroundColor = style.assets.colors.selected
        layer.borderColor = style.assets.colors.primaryContent.cgColor
        layer.borderWidth = Constants.borderWidth
        layer.cornerRadius = bounds.width / 2
        layer.masksToBounds = true

        // Add Shadow
        layer.shadowColor = style.assets.colors.defaultShadow.cgColor
        layer.shadowOpacity = 1
        layer.shadowOffset = .zero
        layer.shadowPath = UIBezierPath(rect: bounds).cgPath
        layer.shadowRadius = bounds.width / 2
    }

    func animateUp() {
        UIView.animate(withDuration: 0.15,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
                        self.transform = .identity
        }, completion: nil)
    }

    func animateDown() {
        UIView.animate(withDuration: 0.15,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
                        self.transform = .init(scaleX: 0.95, y: 0.95)
        }, completion: nil)
    }
}
