//
//  FunctionalView.swift
//  Snapy
//
//  Created by Игорь Талов on 26/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let borderWidth: CGFloat = 4.0
    static let side: CGFloat = 40.0
    static let iconMargin: CGFloat = 11.0
}

class FunctionalView: UIView {
    // MARK: - Private properties
    private let style: Style

    private lazy var effectView: UIVisualEffectView = {
        let view = UIVisualEffectView(frame: .zero)
        view.effect = UIBlurEffect(style: .light)
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()

    private lazy var iconImageView: UIImageView = {
        let imageView = UIImageView(frame: .zero)
        imageView.translatesAutoresizingMaskIntoConstraints = false
        imageView.contentMode = .scaleAspectFill
        return imageView
    }()

    // MARK: - Public properties
    var viewModel: FunctionViewModel? {
        didSet {
            bindViewModel(viewModel: viewModel)
        }
    }

    // MARK: - Initialization
    init(style: Style,
         frame: CGRect) {
        self.style = style
        super.init(frame: frame)
        setupViews()
        setupDecorations()
        setupGesture()
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: Constants.side,
                      height: Constants.side)
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        performMediumImpactHapticFeedback()
        animateDown()
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        animateUp()
    }
}

private extension FunctionalView {
    func setupViews() {
        addSubview(effectView)
        addSubview(iconImageView)

        NSLayoutConstraint.activate([
            effectView.leadingAnchor.constraint(equalTo: leadingAnchor),
            effectView.trailingAnchor.constraint(equalTo: trailingAnchor),
            effectView.topAnchor.constraint(equalTo: topAnchor),
            effectView.bottomAnchor.constraint(equalTo: bottomAnchor),
            iconImageView.leadingAnchor.constraint(equalTo: leadingAnchor, constant: Constants.iconMargin),
            iconImageView.trailingAnchor.constraint(equalTo: trailingAnchor,
                                                    constant: -Constants.iconMargin),
            iconImageView.topAnchor.constraint(equalTo: topAnchor,
                                               constant: Constants.iconMargin),
            iconImageView.bottomAnchor.constraint(equalTo: bottomAnchor,
                                                  constant: -Constants.iconMargin)
        ])
    }

    func setupDecorations() {
        layer.cornerRadius = Constants.side / 2
        layer.masksToBounds = true

        iconImageView.tintColor = style.assets.colors.primaryText
    }

    func setupGesture() {
        let gesture = UITapGestureRecognizer(target: self,
                                             action: #selector(didTapAction))
        addGestureRecognizer(gesture)
    }

    func imageForType(type: FunctionViewModel.FunctionType) -> UIImage? {
        switch type {
        case .whiteBalance:
            return style.assets.images.temperature
        case .flash:
            return style.assets.images.flashOnIcon
        case .grid:
            return style.assets.images.gridIcon
        case .toggleCamera:
            return style.assets.images.cameraToggleIcon
        case .shutter:
            return nil
        }
    }

    func animateUp() {
        UIView.animate(withDuration: 0.15,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
                        self.transform = .identity
        }, completion: nil)
    }

    func animateDown() {
        UIView.animate(withDuration: 0.15,
                       delay: 0.0,
                       options: [.curveEaseOut],
                       animations: {
                        self.transform = .init(scaleX: 0.95, y: 0.95)
        }, completion: nil)
    }

    func bindViewModel(viewModel: FunctionViewModel?) {
        guard let viewModel = viewModel else {
            return
        }

        if viewModel.type == .shutter {
            effectView.isHidden = true
            backgroundColor = style.assets.colors.secondary
            layer.borderColor = style.assets.colors.primaryContent.cgColor
            layer.borderWidth = Constants.borderWidth
            return
        }

        iconImageView.image = imageForType(type: viewModel.type)
    }
}

private extension FunctionalView {
    @objc
       func didTapAction() {
           guard viewModel != nil else {
               return
           }
           viewModel?.action?()
       }
}
