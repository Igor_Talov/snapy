//
//  GridLayer.swift
//  Snapy
//
//  Created by Игорь Талов on 22/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let numberOfColumns: Int = 3
    static let numberOfRows: Int = 3
    static let gridWidth: CGFloat = 0.5
}

final class GridLayer: CALayer {
    // MARK: - Private properties
    private let shapeLayer = CAShapeLayer()
    private let appStyle: Style

    init(style: Style,
         frame: CGRect) {
        self.appStyle = style
        super.init()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

private extension GridLayer { }
