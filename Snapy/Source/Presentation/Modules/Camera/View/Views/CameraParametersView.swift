//
//  CameraParametersView.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let stackViewSpacing: CGFloat = 20.0
}

class CameraParametersView: UIView {
    // MARK: - Public properties
    var style: Style?

    // MARK: - Private properties
    private lazy var stackView: UIStackView = {
        let stackView = UIStackView(frame: .zero)
        stackView.translatesAutoresizingMaskIntoConstraints = false
        stackView.axis = .vertical
        stackView.distribution = .fillEqually
        stackView.spacing = Constants.stackViewSpacing
        return stackView
    }()

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension CameraParametersView {
    func setupViews() {
        addSubview(stackView)
        NSLayoutConstraint.activate([
            stackView.leadingAnchor.constraint(equalTo: leadingAnchor),
            stackView.trailingAnchor.constraint(equalTo: trailingAnchor),
            stackView.topAnchor.constraint(equalTo: topAnchor),
            stackView.bottomAnchor.constraint(equalTo: bottomAnchor)
        ])
    }

    func updateElementsStyle() {

    }
}
