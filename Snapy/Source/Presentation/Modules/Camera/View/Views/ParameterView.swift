//
//  ParameterView.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

private struct Constants {
    static let borderWidth: CGFloat = 0.2
    static let cornerRadius: CGFloat = 91.0
    static let heighConstraint: CGFloat = 40.0
    static let widthConstraint: CGFloat = 90.0
}

class ParameterView: UIView {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: ParameterViewModel? {
        didSet {
            bindViewModel()
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var titleLabel: UILabel? {
        didSet {
            updateTitleLabelStyle()
        }
    }

    @IBOutlet private weak var valueLabel: UILabel? {
        didSet {
            updateValueLabelStyle()
        }
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        updateElementsStyle()
    }

    override var intrinsicContentSize: CGSize {
        return CGSize(width: Constants.widthConstraint,
                      height: Constants.heighConstraint)
    }
}

private extension ParameterView {
    func updateElementsStyle() {
        updateTitleLabelStyle()
        updateValueLabelStyle()
        updateViewStyle()
    }

    func updateViewStyle() {
        layer.borderColor = style?.assets.colors.primaryContent.cgColor
        layer.borderWidth = Constants.borderWidth
        layer.cornerRadius = bounds.height / 2
        layer.masksToBounds = true
    }

    func updateTitleLabelStyle() {
        titleLabel?.textColor = style?.assets.colors.primaryText
        titleLabel?.font = style?.assets.fonts.footnote
    }

    func updateValueLabelStyle() {
        valueLabel?.textColor = style?.assets.colors.primaryText
        valueLabel?.font = style?.assets.fonts.caption
    }

    func bindViewModel() {
        guard let viewModel = viewModel else {
            return
        }
        titleLabel?.text = viewModel.title
        valueLabel?.text = viewModel.value
    }
}
