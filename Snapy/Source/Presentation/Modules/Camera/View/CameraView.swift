//
//  CameraView.swift
//  Snapy
//
//  Created by Игорь Талов on 10/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class CameraViewController: UIViewController {
    // MARK: - Private properties
    private let style: Style
    private var cameraOutputView: CameraOutputView?
    private var focusIndicator: FocusIndicator?

    // MARK: - Public properties
    var output: CameraViewOutput?

    // MARK: - Outlets
    @IBOutlet private weak var functionalContainer: UIView?
    @IBOutlet private weak var horizontalStackView: UIStackView?
    @IBOutlet private weak var pickerImageView: UIImageView?
    @IBOutlet private weak var closeButton: UIButton? {
        didSet {
            closeButton?.tintColor = style.assets.colors.primaryText
            closeButton?.setImage(style.assets.images.closeIcon, for: .normal)
        }
    }

    @IBOutlet private weak var controlView: UIView?

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
        super.init(nibName: String(describing: type(of: self)),
                   bundle: nil)
    }

    override func viewDidLoad() {
        super.viewDidLoad()
        output?.viewDidLoad()
    }

    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func viewWillLayoutSubviews() {
        super.viewWillLayoutSubviews()
        cameraOutputView?.frame = view.bounds
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        let screenSize = view.bounds.size
        if let touchPoint = touches.first?.location(in: self.view) {
            let focusOriginX = touchPoint.y / screenSize.height
            let focusOriginY = 1.0 - touchPoint.x / screenSize.width
            let focusPoint = CGPoint(x: focusOriginX, y: focusOriginY)
            output?.didTapFocus(focusPoint: focusPoint,
                                touchPoint: touchPoint)
        }
    }
}

private extension CameraViewController {

    @IBAction func didTapCloseButton(_ sender: UIButton) {
        performLightImpactHapticFeedback()
        output?.didTapCloseButton()
    }
}

extension CameraViewController: CameraViewInput {
    func displayFunctionButtons(viewModels: [FunctionViewModel]) {
        safelyRunUICode {
            for viewModel in viewModels {
                let view = self.makeFunctionalView(for: viewModel)
                self.horizontalStackView?.addArrangedSubview(view)
            }
            self.bringViewsToFront()
        }

    }

    func displayPickerImage(mediaPickerItem: MediaPickerItem) {
        safelyRunUICode {
            let imageSource = mediaPickerItem.image
            self.pickerImageView?.setImage(fromSource: imageSource)
        }
    }

    func displayFocus(onPoint focusPoint: CGPoint) {
        safelyRunUICode {
            self.focusIndicator?.hide()
            self.focusIndicator = FocusIndicator(style: self.style)
            self.focusIndicator?.animate(in: self.view.layer, focusPoint: focusPoint)
        }
    }

    func setEVParameters(parameters: ParameterViewModel) { }

    func setISOParameters(parameters: ParameterViewModel) { }

    func setShutterSpeedParameters(parameters: ParameterViewModel) { }

    func setCameraOutputParameters(parameters: CameraOutputParameters) {
        safelyRunUICode {
            let newCameraOutputView = CameraOutputView(captureSession: parameters.captureSession,
                                                       outputOrientation: parameters.orientation)
            self.cameraOutputView?.removeFromSuperview()
            self.view.addSubview(newCameraOutputView)
            self.bringViewsToFront()
            self.cameraOutputView = newCameraOutputView
            self.viewWillLayoutSubviews()
        }
    }
}

private extension CameraViewController {

    func makeFunctionalView(for viewModel: FunctionViewModel) -> FunctionalView {
        let view = FunctionalView(style: style, frame: .zero)
        view.translatesAutoresizingMaskIntoConstraints = false

        NSLayoutConstraint.activate([
            view.widthAnchor.constraint(equalToConstant: view.intrinsicContentSize.width),
            view.heightAnchor.constraint(equalToConstant: view.intrinsicContentSize.height)
        ])
        view.viewModel = viewModel

        return view
    }

    func bringViewsToFront() {
        guard let pickerImageView = pickerImageView,
            let controlView = controlView,
            let horizontalStackView = horizontalStackView,
            let functionalContainer = functionalContainer else {
            return
        }

        self.view.bringSubviewToFront(pickerImageView)
        self.view.bringSubviewToFront(controlView)
        self.view.bringSubviewToFront(functionalContainer)
        self.view.bringSubviewToFront(horizontalStackView)
    }
}
