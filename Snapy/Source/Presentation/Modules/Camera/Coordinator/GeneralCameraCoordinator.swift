//
//  GeneralCameraCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralCameraCoordinator: CameraCoordinator {
    // MARK: - Private properties
    private let style: Style
    private(set) var isStarted = false
    private let moduleFactory: CameraModuleFactory

    private(set) var rootViewController: UIViewController?

    // MARK: - Public properties
    var onDismiss: (() -> Void)?

    init(style: Style,
         moduleFactory: CameraModuleFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
    }

    func start() -> UIViewController {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return UIViewController()
        }

        isStarted = true

        let module = moduleFactory.makeCameraModule(output: self)
        module.input.start()
        let controller = module.rootViewController
        rootViewController = controller
        return controller
    }

}

extension GeneralCameraCoordinator: CameraModuleOutput {
    func cameraModuleDidDismissRequest(_ module: CameraModuleInput) {
        onDismiss?()
    }
}
