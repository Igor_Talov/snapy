//
//  CameraCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

protocol CameraCoordinator: Coordinator {

    var onDismiss: (() -> Void)? { get set }

    func start() -> UIViewController
}
