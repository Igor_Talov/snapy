//
//  CameraModuleIO.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

protocol CameraModuleInput: class {
    func start()
}

protocol CameraModuleOutput: class {
    func cameraModuleDidDismissRequest(_ module: CameraModuleInput)
}
