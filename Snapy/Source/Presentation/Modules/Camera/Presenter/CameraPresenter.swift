//
//  CameraPresenter.swift
//  Snapy
//
//  Created by Игорь Талов on 16/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreGraphics
import Foundation

class CameraPresenter {
    // MARK: - Private properties
    private let interactor: CameraInteractorInput

    // MARK: - Public properties
    weak var output: CameraModuleOutput?

    private var view: CameraViewInput?

    // MARK: - Initialization
    init(interactor: CameraInteractorInput,
         view: CameraViewInput) {
        self.interactor = interactor
        self.view = view
    }
}

extension CameraPresenter: CameraModuleInput {
    func start() { }
}

extension CameraPresenter: CameraViewOutput {
    func didTapFocus(focusPoint: CGPoint, touchPoint: CGPoint) {
        if interactor.focusCameraOnPoint(focusPoint: focusPoint) {
            view?.displayFocus(onPoint: touchPoint)
        }
    }

    func didTapCloseButton() {
        output?.cameraModuleDidDismissRequest(self)
    }

    func didTapShutterButton() {
        interactor.takePhoto { [weak self] item in
            guard let self = self else {
                return
            }

            guard let item = item else {
                return
            }

            self.view?.displayPickerImage(mediaPickerItem: item)
        }
    }

    func viewDidLoad() {
        setupView()
    }
}

private extension CameraPresenter {
    func setupView() {
        interactor.getOutputParameters { [weak self] parameters in
            guard let self = self else {
                return
            }

            guard let parameters = parameters else {
                return
            }

            self.view?.setCameraOutputParameters(parameters: parameters)
        }
        let functionalViewModel = makeFunctionButtonViewModels()
        self.view?.displayFunctionButtons(viewModels: functionalViewModel)
    }

    func makeFunctionButtonViewModels() -> [FunctionViewModel] {
        let wbAction = {
            // Change white balance
            print("White Balance")
        }

        let gridAction = {
            // Display grid
            print("Display grid")
        }

        let toggleCameraAction = {
            // Toggle Camera
            print("Toggle Camera")
        }

        let toggleFlashAction = {
            // Toggle flash
            print("Toggle Flash")
        }

        let shutterAction = { [weak self] in
            guard let self = self else {
                return
            }
            self.didTapShutterButton()
        }
        let wbFunctionViewModel = FunctionViewModel(type: .whiteBalance,
                                                    action: wbAction)
        let gridFunctionViewModel = FunctionViewModel(type: .grid,
                                                      action: gridAction)
        let toggleCameraViewModel = FunctionViewModel(type: .toggleCamera,
                                                      action: toggleCameraAction)
        let flashViewModel = FunctionViewModel(type: .flash,
                                               action: toggleFlashAction)
        let shutterViewModel = FunctionViewModel(type: .shutter,
                                                 action: shutterAction)

        let viewModels = [wbFunctionViewModel,
                          gridFunctionViewModel,
                          toggleCameraViewModel,
                          flashViewModel,
                          shutterViewModel
        ]

        return viewModels
    }
}
