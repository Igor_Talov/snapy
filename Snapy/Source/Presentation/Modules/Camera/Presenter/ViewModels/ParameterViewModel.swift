//
//  ParameterViewModel.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

struct ParameterViewModel {
    let title: String
    let value: String
    let type: ParameterType
}

extension ParameterViewModel {
    enum ParameterType {
        case exposure
        case shutterSpeed
        case iso
    }
}
