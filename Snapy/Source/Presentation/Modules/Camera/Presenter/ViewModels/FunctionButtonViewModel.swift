//
//  FunctionButtonViewModel.swift
//  Snapy
//
//  Created by Игорь Талов on 26/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

struct FunctionViewModel {
    let type: FunctionType
    let action: (() -> Void)?
}

extension FunctionViewModel {
    enum FunctionType {
        case whiteBalance
        case grid
        case toggleCamera
        case flash
        case shutter
    }
}
