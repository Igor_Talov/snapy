//
//  GeneralCameraModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralCameraModuleFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralCameraModuleFactory: CameraModuleFactory {
    func makeCameraModule(output: CameraModuleOutput) -> ModuleInterface<CameraModuleInput> {
        let builder = CameraModuleBuilder(style: style)
        return builder.buildModule(with: output)
    }
}
