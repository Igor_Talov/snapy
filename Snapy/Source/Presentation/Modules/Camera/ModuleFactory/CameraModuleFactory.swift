//
//  CameraModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol CameraModuleFactory {
    func makeCameraModule(output: CameraModuleOutput)
        -> ModuleInterface<CameraModuleInput>
}
