//
//  CameraModuleBuilder.swift
//  Snapy
//
//  Created by Игорь Талов on 17/09/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class CameraModuleBuilder {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func buildModule(with output: CameraModuleOutput) -> ModuleInterface<CameraModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)

        presenter.output = output
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension CameraModuleBuilder {
    func makeView() -> CameraViewController {
        return CameraViewController(style: style)
    }

    func makePresenter(view: CameraViewInput,
                       interactor: CameraInteractorInput) -> CameraPresenter {
        let presenter = CameraPresenter(interactor: interactor,
                                        view: view)
        return presenter
    }

    func makeInteractor() -> CameraInteractor {
        let imageStorageService = GeneralImageStorageService()
        let cameraService = GeneralCameraService(cameraType: .back,
                                                 imageStorageService: imageStorageService)
        let interactor = CameraInteractor(cameraService: cameraService)
        return interactor
    }
}
