//
//  FilterCollectionViewCell.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class FilterCollectionViewCell: UICollectionViewCell {
    // MARK: - Public properties
    var style: Style? {
        didSet {
            updateElementsStyle()
        }
    }

    var viewModel: FilterViewModel? {
        didSet {
            bindViewModel(viewModel: viewModel)
        }
    }

    // MARK: - Outlets
    @IBOutlet private weak var imageView: UIImageView?
    @IBOutlet private weak var titleContainer: UIView?
    @IBOutlet private weak var titleLabel: UILabel?

    override func awakeFromNib() {
        super.awakeFromNib()
    }
}

private extension FilterCollectionViewCell {
    func updateElementsStyle() {
        updateTitleLabelStyle()
    }

    func updateTitleLabelStyle() {
        titleLabel?.textColor = style?.assets.colors.primaryContent
        titleLabel?.font = style?.assets.fonts.footnote
    }

    func bindViewModel(viewModel: FilterViewModel?) {
        guard let viewModel = viewModel else {
            return
        }

        titleLabel?.text = viewModel.title
        imageView?.image = viewModel.preview
    }
}
