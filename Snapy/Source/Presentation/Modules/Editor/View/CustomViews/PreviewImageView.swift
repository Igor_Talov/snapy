//
//  PreviewImageView.swift
//  Snapy
//
//  Created by Игорь Талов on 08/02/2020.
//  Copyright © 2020 Игорь Талов. All rights reserved.
//

import UIKit

class PreviewImageView: UIView {

    // MARK: - Private properties
    private let originalImageView = UIImageView()
    private let imageView = UIImageView()

    // MARK: - Public properties
    var originalImage: CIImage? {
        didSet {
            guard oldValue != originalImage else { return }
            originalImageView.image = originalImage
                .flatMap { $0.transformed(by: .init(translationX: -$0.extent.origin.x, y: -$0.extent.origin.y)) }
                .flatMap { UIImage(ciImage: $0, scale: UIScreen.main.scale, orientation: .up) }
        }
    }

    var image: CIImage? {
        didSet {
            guard oldValue != image else { return }
            imageView.image = image
                .flatMap { $0.transformed(by: .init(translationX: -$0.extent.origin.x, y: -$0.extent.origin.y)) }
                .flatMap { UIImage(ciImage: $0, scale: UIScreen.main.scale, orientation: .up) }
        }
    }

    // MARK: - Initialization
    override init(frame: CGRect) {
        super.init(frame: .zero)
        setupViews()
    }

    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }

    override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesBegan(touches, with: event)
        originalImageView.isHidden = false
        imageView.isHidden = true
    }

    override func touchesEnded(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesEnded(touches, with: event)
        originalImageView.isHidden = true
        imageView.isHidden = false
    }

    override func touchesCancelled(_ touches: Set<UITouch>, with event: UIEvent?) {
        super.touchesCancelled(touches, with: event)
        originalImageView.isHidden = true
        imageView.isHidden = false
    }
}

private extension PreviewImageView {
    func setupViews() {
        [originalImageView, imageView].forEach { imageView in
                addSubview(imageView)
                imageView.contentMode = .scaleAspectFill
                imageView.frame = bounds
                imageView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        }

        originalImageView.isHidden = true
    }
}
