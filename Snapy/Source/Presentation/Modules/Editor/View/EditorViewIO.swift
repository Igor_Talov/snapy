//
//  EditorViewIO.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage

protocol EditorViewInput: class {
    func displayFilters(viewModels: [FilterViewModel])
    func displayTools()
    func displaySourceImage(sourceImage: CIImage)
}

protocol EditorViewOutput: class {
    // MARK: - Lifecycle
    func viewDidLoad()

    // MARK: - Actions
    func didTapDoneButton()
    func didTapCloseButton()
}
