//
//  EditorViewModelFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 23/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import Foundation

protocol EditorViewModelFactory {
    func makeFilterPreviewViewModel(filter: ColorCubeFilter,
                                    sourceImage: CIImage,
                                    action: ((ColorCubeFilter) -> Void)?) -> FilterViewModel
}
