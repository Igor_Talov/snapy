//
//  EditorModuleIO.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage

// MARK: - EditorModuleInput
protocol EditorModuleInput: class {
    func start(with image: CIImage)
}

// MARK: - EditorModuleOutput
protocol EditorModuleOutput: class { }
