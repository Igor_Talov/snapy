//
//  GeneralEditorViewModelFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 23/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import UIKit

class GeneralEditorViewModelFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralEditorViewModelFactory: EditorViewModelFactory {
    func makeFilterPreviewViewModel(filter: ColorCubeFilter,
                                    sourceImage: CIImage,
                                    action: ((ColorCubeFilter) -> Void)?) -> FilterViewModel {
        let previewCIImage = filter.apply(to: sourceImage,
                                          sourceImage: sourceImage)
        let previewImage = UIImage(ciImage: previewCIImage!)
        let filterViewModel = FilterViewModel(title: filter.name,
                                              preview: previewImage,
                                              filter: filter,
                                              action: action)
        return filterViewModel
    }
}
