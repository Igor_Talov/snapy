//
//  FilterViewModel.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

struct FilterViewModel {
    let title: String
    let preview: UIImage?
    let filter: ColorCubeFilter
    let action: ((ColorCubeFilter) -> Void)?
}
