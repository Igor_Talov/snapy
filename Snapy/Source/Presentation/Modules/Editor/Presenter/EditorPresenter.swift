//
//  EditorPresenter.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import Foundation

private struct State {
    var image: CIImage?
}

class EditorPresenter {
    // MARK: - Private properties
    private weak var view: EditorViewInput?
    private let interactor: EditorInteractorInput
    private let viewModelfactory: EditorViewModelFactory
    private var state = State(image: nil)

    weak var output: EditorModuleOutput?

    init(view: EditorViewInput,
         interactor: EditorInteractorInput,
         viewModelfactory: EditorViewModelFactory) {
        self.view = view
        self.interactor = interactor
        self.viewModelfactory = viewModelfactory
    }
}

extension EditorPresenter: EditorViewOutput {
    func viewDidLoad() {
        guard let image = state.image else {
            return
        }
        interactor.fetchFilters()
        view?.displaySourceImage(sourceImage: image)
    }

    func didTapDoneButton() {

    }

    func didTapCloseButton() {

    }
}

extension EditorPresenter: EditorModuleInput {
    func start(with image: CIImage) {
        state.image = image
    }
}

extension EditorPresenter: EditorInteractorOutput {
    func didRequestApply(resultPhoto: CIImage) {
        // TODO: Make correct naming for variables & methods 🤔
        view?.displaySourceImage(sourceImage: resultPhoto)
    }

    func didFetchFilters(filters: [ColorCubeFilter]) {
        guard let sourceImage = state.image else {
            return
        }

        let action: (ColorCubeFilter) -> Void = { [weak self] filter in
            guard let self = self else {
                return
            }
            self.interactor.requestApply(filter: filter,
                                         for: sourceImage)
        }
        let viewModels = filters.map {
            return viewModelfactory
                .makeFilterPreviewViewModel(filter: $0,
                                            sourceImage: sourceImage,
                                            action: action)
        }
        view?.displayFilters(viewModels: viewModels)
    }
}
