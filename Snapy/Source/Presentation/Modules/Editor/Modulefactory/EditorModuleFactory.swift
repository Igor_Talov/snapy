//
//  EditorModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 26/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

protocol EditorModuleFactory {
    func makeEditorModule(output: EditorModuleOutput) -> ModuleInterface<EditorModuleInput>
}
