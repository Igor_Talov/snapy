//
//  GeneralEditorModuleFactory.swift
//  Snapy
//
//  Created by Игорь Талов on 26/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class GeneralEditorModuleFactory {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }
}

extension GeneralEditorModuleFactory: EditorModuleFactory {
    func makeEditorModule(output: EditorModuleOutput) -> ModuleInterface<EditorModuleInput> {
        let builder = EditorModuleBuilder(style: style)
        return builder.buildModule(with: output)
    }
}
