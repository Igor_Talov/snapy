//
//  GeneralEditorCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 26/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

class GeneralEditorCoordinator: EditorCoordinator {
    // MARK: - Private properties
    private let style: Style
    private let moduleFactory: EditorModuleFactory
    private(set) var isStarted = false
    private var rootController: UIViewController?
    private var childCoordinators: [Coordinator] = []

    var onDismiss: (() -> Void)?

    init(style: Style,
         moduleFactory: EditorModuleFactory) {
        self.style = style
        self.moduleFactory = moduleFactory
    }

    func start(with editorData: EditorData) -> UIViewController {
        guard !isStarted else {
            assertionFailure("\(self) is already started")
            return UIViewController()
        }

        let module = moduleFactory.makeEditorModule(output: self)
        module.input.start(with: editorData.ciimage)
        let rootController = module.rootViewController
        self.rootController = rootController

        return rootController
    }
}

extension GeneralEditorCoordinator: EditorModuleOutput {

}
