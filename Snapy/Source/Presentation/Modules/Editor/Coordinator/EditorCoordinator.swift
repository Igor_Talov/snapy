//
//  EditorCoordinator.swift
//  Snapy
//
//  Created by Игорь Талов on 26/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import UIKit

struct EditorData {
    let ciimage: CIImage
}

protocol EditorCoordinator: Coordinator {

    var onDismiss: (() -> Void)? { get set }

    func start(with editorData: EditorData) -> UIViewController
}
