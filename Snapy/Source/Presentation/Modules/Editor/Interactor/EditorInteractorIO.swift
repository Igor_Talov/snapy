//
//  EditorInteractorIO.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage

protocol EditorInteractorInput: class {
    func fetchFilters()
    func requestApply(filter: Filterable, for sourceImage: CIImage)
}

protocol EditorInteractorOutput: class {
    func didFetchFilters(filters: [ColorCubeFilter])
    func didRequestApply(resultPhoto: CIImage)
}
