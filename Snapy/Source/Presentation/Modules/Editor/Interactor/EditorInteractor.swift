//
//  EditorInteractor.swift
//  Snapy
//
//  Created by Игорь Талов on 20/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import CoreImage
import Foundation

private class FilterOperation: Operation {
    // MARK: - Private properties
    private var sourceImage: CIImage
    private var filter: Filterable

    override var isAsynchronous: Bool {
        return true
    }

    // MARK: - Public properties
    var outputImage: CIImage?

    // MARK: - Initialization
    init(sourceImage: CIImage,
         filter: Filterable) {
        self.sourceImage = sourceImage
        self.filter = filter
    }

    // MARK: - Main
    override func main() {
        outputImage = filter.apply(to: sourceImage, sourceImage: sourceImage)
    }
}

class EditorInteractor {
    // MARK: - Private properties
    private var filterService: FilterService

    // MARK: - Public proprties
    weak var output: EditorInteractorOutput?
    let filterQueue = OperationQueue(name: "com.snapy.editorInteractor.filterQueue")

    init(filterService: FilterService) {
        self.filterService = filterService
    }
}

extension EditorInteractor: EditorInteractorInput {
    func requestApply(filter: Filterable, for sourceImage: CIImage) {
        let filterOperation = FilterOperation(sourceImage: sourceImage,
                                              filter: filter)
        filterOperation.completionBlock = { [weak self] in
            guard let self = self else {
                return
            }
            guard let processedImage = filterOperation.outputImage else {
                assertionFailure("output image is nil")
                return
            }
            self.output?.didRequestApply(resultPhoto: processedImage)
        }
        filterQueue.addOperation(filterOperation)
    }

    func fetchFilters() {
        let filters = filterService.getColorCubeFilters()
        output?.didFetchFilters(filters: filters)
    }
}
