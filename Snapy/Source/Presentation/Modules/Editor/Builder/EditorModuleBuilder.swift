//
//  EditorModuleBuilder.swift
//  Snapy
//
//  Created by Игорь Талов on 26/10/2019.
//  Copyright © 2019 Игорь Талов. All rights reserved.
//

import Foundation

class EditorModuleBuilder {
    // MARK: - Private properties
    private let style: Style

    // MARK: - Initialization
    init(style: Style) {
        self.style = style
    }

    func buildModule(with output: EditorModuleOutput) -> ModuleInterface<EditorModuleInput> {
        let view = makeView()
        let interactor = makeInteractor()
        let presenter = makePresenter(view: view,
                                      interactor: interactor)
        presenter.output = output
        interactor.output = presenter
        view.output = presenter
        return ModuleInterface(input: presenter, rootViewController: view)
    }
}

private extension EditorModuleBuilder {
    func makeView() -> EditorViewController {
        return EditorViewController(style: style)
    }

    func makePresenter(view: EditorViewInput,
                       interactor: EditorInteractor) -> EditorPresenter {
        let viewModelFactory = GeneralEditorViewModelFactory(style: style)
        let presenter = EditorPresenter(view: view,
                                        interactor: interactor,
                                        viewModelfactory: viewModelFactory)
        return presenter
    }

    func makeInteractor() -> EditorInteractor {
        let filterService = GeneralFilterService(filtersStorage: ColorCubeStorage.default)
        let interactor = EditorInteractor(filterService: filterService)
        return interactor
    }
}
